#include <stdbool.h>
#include <stdio.h>

// function prototype.
bool checkSpaces( int class,  int* seats);
void initArray(int size,int* seats);
void displayArray(int size,int* seats);

int main(int argc, const char * argv[])
{
    int size=10;
    int seats[size];
    int cat;
    int outOfSpaceE=0,outOfSpaceF=0;
    char change;
    //Replaced initialice code for functions
    initArray(size,seats);
    displayArray(size,seats);

do {
    printf("\nPlease type 1 for \"first class\"");
    printf("\nPlease type 2 for \"economy\"\n");

    scanf("%d", &cat);

    switch (cat) {
        case 1:
            if (checkSpaces(cat, seats)){
                seats[outOfSpaceF]=1;
                printf("\nYour seat is %d in first class\n\n",outOfSpaceF+1);
                outOfSpaceF++;

            }else{
                printf("\nIs it acceptable to be placed in the economy section\n Y/N\n");
                scanf("%s", &change);
                if (change=='Y' || change=='y'){
                    cat=2;
                    if(checkSpaces(cat, seats)){
                        seats[outOfSpaceE+5]=1;
                        printf("Your seat is %d in economy\n\n",outOfSpaceE+6);
                        outOfSpaceE++;
                    }
                    else{
                        printf("Next flight leaves in 3 hours.\n\n");
                    }
                }
                else{
                    printf("Next flight leaves in 3 hours.\n\n");
                }
            }
            break;
        case 2:
            if (checkSpaces(cat, seats)){
                seats[outOfSpaceE+5]=1;
                printf("\nYour seat is %d in economy\n\n",outOfSpaceE+6);
                outOfSpaceE++;

                //Your program should then print a boarding pass indicating the person's seat number and whether it’s in the first class or economy section of the plane.
            }else{
                printf("\nIs it acceptable to be placed in the first class section\nY/N\n");
                scanf("%s", &change);
                if (change=='Y' || change=='y'){
                    cat=1;
                    if(checkSpaces(cat, seats)){
                        seats[outOfSpaceF]=1;
                        printf("\nYour seat is %d in first class\n\n",outOfSpaceF+1);
                        outOfSpaceF++;

                    }
                    else{
                        printf("Next flight leaves in 3 hours.\n\n");
                    }
                }
                else{
                    printf("Next flight leaves in 3 hours.\n\n");
                }
            }
            break;

        default:
            break;
    }
     displayArray(size,seats);
 }while(outOfSpaceE+outOfSpaceF<10);
    return 0;
}

bool checkSpaces( int class,  int* seats){
    switch (class) {
        case 1:
            for(int i=0; i<5;i++){
                if(seats[i]==0){
                    return true;
                }
            }
            break;
        case 2:
            for(int i=5; i<10;i++){
                if(seats[i]==0){
                    return true;
                }
            }
            break;
    }

    return false;
}


void initArray(int size,int* seats){
    for(int i=0;i<size; i++){
        seats[i]=0;
    }
}

void displayArray(int size,int* seats){
    for(int i=0;i<size; i++){
        printf("%d ",seats[i]);
    }
}

